# p13n fork of TeXstudio

## features
- https://github.com/texstudio-org/texstudio/pull/2503

## workflow
- source diff is tracked in the github fork: https://github.com/bryango/texstudio/tree/master
- use the github fork to generate a patch file
- fork is only updated _when necessary_, such that the patch applies well
- however, this repo is updated _constantly_, to track the Arch upstream
