# Maintainer: Sven-Hendrik Haase <svenstaro@archlinux.org>
# Contributor: Clément DEMOULINS <clement@archivel.fr>
pkgname=texstudio
pkgver=4.7.2
pkgrel=1
epoch=1
pkgdesc="Integrated writing environment for creating LaTeX documents"
arch=('x86_64')
url="http://texstudio.sourceforge.net/"
license=('GPL')
depends=('poppler-qt6' 'qt6-svg' 'libxkbcommon-x11' 'hicolor-icon-theme' 'hunspell' 'quazip-qt6' 'desktop-file-utils'
         'qt6-declarative' 'qt6-5compat')
makedepends=('qt6-tools' 'mercurial' 'imagemagick' 'librsvg' 'ninja' 'cmake')
optdepends=('evince: pdf reader'
            'languagetool: spelling and grammar checking'
            'okular: alternate pdf reader')
replaces=('texmakerx')
source=("$pkgname-$pkgver.tar.gz::https://github.com/texstudio-org/texstudio/archive/${pkgver}.tar.gz"
        fs54269.patch
        fs72345.patch
        "bryango.patch::https://github.com/texstudio-org/texstudio/compare/bryango:texstudio:master.patch")
sha512sums=('cadfcafc4a00cf4a999b7707a5216ba4b6a2184f8c542b1394739cc8c413577c641268cae86647eb3901b048dd31c85b0f98b5532d52c01baec01446674f6380'
            '247e85f668b06c8a2def4e7456f0ddc8d35c7990484c1c037f7cf43fdee91419b5df66e968ef1e0c637fe685722bbad9171bdd79fad7a10912ae329aff088b68'
            '6975eebd43b32fb7aa04f973562e66f6a83df0fb6a0b4d8221e124c9cc6959df11ec517e4c862921619c8b1c55800257017e26001c03186b84c567fc19c98726'
            '0da89f7bd79e3157341d61823fd47accb773e09da7d53a2df2a0a2180b82eeeb6316bb9dd39e69040b9f444933e2c41f5ae380060e260c93a6be283a6f51e92f')

prepare() {
    cd texstudio-${pkgver}/src
    patch -Np1 -i "${srcdir}"/fs54269.patch
    patch -Np1 -i "${srcdir}"/fs72345.patch
    patch -Np2 -i "${srcdir}"/bryango.patch
}

build() {
    cmake -B build -S texstudio-${pkgver} \
        -GNinja \
        -DCMAKE_BUILD_TYPE=None \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -Wno-dev

    ninja -C build
}

package() {
    DESTDIR="${pkgdir}" ninja -C build install

    for res in $(/bin/ls /usr/share/icons/hicolor | grep '[0-9]\+x[0-9]\+'); do
        mkdir -p "${pkgdir}"/usr/share/icons/hicolor/${res}/apps
        convert +set date:create +set date:modify -background none -density 1200 -resize ${res} \
            "${pkgdir}"/usr/share/icons/hicolor/scalable/apps/texstudio.svg \
            "${pkgdir}"/usr/share/icons/hicolor/${res}/apps/texstudio.png
    done

    # Remove the included dictionaries in favor of hunspell (FS#78778)
    rm -rf "${pkgdir}"/usr/share/texstudio/{*.dic,*.aff}
}
